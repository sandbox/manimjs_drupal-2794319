jQuery(function () {
  jQuery(".q-rbtn").change(function () {
    var current_value = jQuery(this).val();
    var question_score = jQuery(this).attr("name") + "_score";
    jQuery("#" + question_score).val(current_value);
    update_total_score();
  });

  jQuery('.btn-pdf-submit').click(function () {
    jQuery('#action_type').val('pdf');
    validate_questions();
    if (validate_questions()) {
      if (jQuery('#total_score').val() != "") {
        jQuery('#hidden-save').click();
      } else {
        alert('Please select options for all the questions.');
        return false;
      }
    }
  });
});

function validate_questions() {
  var return_flag = true;
  jQuery(".question-score").each(function () {

    var question_index = jQuery(this).attr('rel');

    if (jQuery(this).val() == "") {
      jQuery('#err_' + question_index).html('Please select an answer for this question');
      return_flag = false;
    } else {
      jQuery('#err_' + question_index).html('');
    }
  });

  return return_flag;
}

function update_total_score()
{
  var total_score = 0;
  var val = 0;
  jQuery(".question-score").each(function () {
    val = jQuery(this).val();
    if (val)
    {
      total_score += parseInt(jQuery(this).val());
    }
  });

  if (total_score || total_score == 0)
  {
    jQuery("#total_score").val(total_score);
  }
  else
  {
    jQuery("#total_score").val("");
  }
}

function getUpdateHtml() {
  jQuery('#action_type').val('print');
  validate_questions();
  if (validate_questions()) {
    jQuery.ajax({
      url: window.location.toString(),
      data: jQuery('#frm_calc').serialize(),
      type: 'post'
    }).done(function (r) {
      if (r == 'error') {
        window.location = '<?php echo get_site_url(); ?>';
      } else {
        Popup(r);
      }
    });
  }
}

function Popup(data)
{
  var mywindow = window.open('', 'PUCAI Calculator', 'scrollbars=1, width=900');

  mywindow.document.write(data);
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  mywindow.close();

  return true;
}